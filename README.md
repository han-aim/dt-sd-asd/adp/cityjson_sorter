# CityJSON sorter

## Running

```sh
cargo run --release -- --help
cargo run --release -- 40cz2.json
```

### Features

1. Roundtrip deserialization/serialization of CityJSON.
2. Sorting CityJSON objects by attribute `bouwjaar`. Sorting on other criteria takes one line of code to change.
3. Applying parallellism while sorting.

## Profiling

### Under macOS

- [`cargo-instruments`](https://crates.io/crates/cargo-instruments)
- [Introduction](https://www.reddit.com/r/rust/comments/b20eca/introducing_cargoinstruments_zerohassle_profiling/)

This will profile for at most one minute.

```sh
cargo instruments --limit 60000 --bin cityjson_sorter --release --open -- 40cz2.json
```

### Under Linux

Using GNU time:

```sh
command time -f 'RSS=%M elapsed=%E cpu.sys=%S cpu.user=%U' target/release/cityjson_sorter 40cz2.json
```

Using `perf`:

```sh
cargo install flamegraph
cargo flamegraph
```

![Flame graph of average run (release build)](flamegraph.svg)

## Data quality issues

### Duplication: `onbegroeidterreindeeloptalud` and `begroeidterreindeeloptalud`

### Varying format: `lv_publicatiedatum` and other date-like fields

```text
lv_publicatiedatum":"20161229235940","
lv_publicatiedatum":"2017-01-13T16:04:
```

### Noise: `documentnummer`

Contains unneeded escape sequences (e.g. `\t`).

### Varying format: `objectid`

Sometimes a string, sometimes an integer.

### Extraneous attribute fields

- `nummeraanduidingreeks_2tekst`
- `numaanduidingreeks_2posit3` (also inconsistently named)
- `nummeraanduidingreeks_1tekst`
- etc.

## Future improvements

### Data representation

#### [Tag by type](https://serde.rs/enum-representations.html)

#### Use [smaller UTF-8 string type](https://www.reddit.com/r/rust/comments/bcviq1/how_can_i_reduce_the_size_of_string_struct/)

#### Do not deserialize as UTF-8 (`&str`) but as bytes (`&[u8]`)

#### Use smaller `Cow` type

[Beef](https://lib.rs/crates/beef)

#### Skip deserializing empty strings

See [`string_empty_as_none`](https://docs.rs/serde_with/1.5.1/serde_with/rust/string_empty_as_none/index.html).

#### Enums instead of strings for `pandstatus`, `3df_class`, etc.

Dededuplicate common strings as enum values.

### Deserialization robustness

Use more efficient parsing, that reuses partial parse.

### Technical

#### [Profile-guided optimization](https://doc.rust-lang.org/rustc/profile-guided-optimization.html)

#### Using CPU-optimized (SIMD) JSON deserializer

[simd-json](https://github.com/simd-lite/simd-json)

##### Use [arena allocation](https://www.reddit.com/r/rust/comments/jddens/announcing_shared_arena_a_threadsafe_memory_pool/)

#### Avoid copying memory

[Intern strings?](https://crates.io/crates/string-interner)

#### Using different memory allocator

[mimalloc](https://github.com/microsoft/mimalloc)
