use super::cityjson::Cityjsonobject;
use std::{
    fs::File,
    io::{BufWriter, Write},
    path::Path,
};

pub fn load_cityjsonobject<'deserialization>(
    path_file_cityjsonobject_in: &'deserialization Path,
    buffer: &'deserialization mut String,
) -> Result<Cityjsonobject<'deserialization>, std::io::Error> {
    *buffer = std::fs::read_to_string(path_file_cityjsonobject_in)?;
    let cityjsonobject: Cityjsonobject =
        serde_json::from_str(buffer.as_str()).expect("Failed to deserialize input JSON. ");
    return Ok(cityjsonobject);
}

pub fn save_cityjsonobject(
    path_file_cityjsonobject_out: &Path,
    cityjsonobject: &Cityjsonobject,
) -> Result<(), std::io::Error> {
    let mut bufwriter = BufWriter::new(File::create(path_file_cityjsonobject_out)?);
    serde_json::to_writer(&mut bufwriter, &cityjsonobject)
        .expect("Failed to serialize input JSON. ");
    return bufwriter.flush();
}
