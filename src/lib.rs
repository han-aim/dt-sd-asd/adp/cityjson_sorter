#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_return)]
#![allow(clippy::missing_errors_doc)]

pub mod cityjson;
pub mod io;
