#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_return)]

mod cityjson;
mod io;

use clap::{App, Arg};
use std::path::Path;

/// # Errors
///
/// When input JSON file could not be read or output JSON file could not be written.
pub fn main() -> Result<(), std::io::Error> {
    let matches = App::new("CityJSON sorter")
        .arg(
            Arg::with_name("INPUT")
                .short("input")
                .long("input")
                .help("Input file path. ")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("OUTPUT")
                .short("output")
                .long("output")
                .help("Output file path. ")
                .required(false)
                .index(2),
        )
        .get_matches();

    if let Some(str_path_file_cityjsonobject_in) = matches.value_of("INPUT") {
        let path_file_cityjsonobject_in = Path::new(str_path_file_cityjsonobject_in);
        let mut buffer = String::new();
        let mut cityjsonobject = io::load_cityjsonobject(path_file_cityjsonobject_in, &mut buffer)?;
        eprintln!("Sorting CityJSON objects in reverse order by `attributes.bouwjaar` ... ");
        cityjsonobject.city_objects.par_sort_by(|_k1, v1, _k2, v2| {
            v1.attributes
                .bouwjaar
                .cmp(&v2.attributes.bouwjaar)
                .reverse()
        });
        let path_file_cityjsonobject_out =
            if let Some(str_path_file_cityjsonobject_out) = matches.value_of("OUTPUT") {
                Path::new(str_path_file_cityjsonobject_out).into()
            } else {
                let mut filename_file_cityjsonobject_out = path_file_cityjsonobject_in
                    .file_stem()
                    .expect("File name does not have a stem. ")
                    .to_owned();
                filename_file_cityjsonobject_out.push("_sorted");
                path_file_cityjsonobject_in
                    .with_file_name(filename_file_cityjsonobject_out)
                    .with_extension(path_file_cityjsonobject_in.extension().unwrap_or_default())
            };
        io::save_cityjsonobject(&path_file_cityjsonobject_out, &cityjsonobject)
    } else {
        panic!("Insufficient command-line arguments provided. ");
    }
}
