use chrono::{NaiveDate, NaiveDateTime};
use deepsize::DeepSizeOf;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

mod bool;
mod float;
mod objectid;
mod realnaivedate;
mod realnaivedatetime;
mod year;

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct Cityjsonobject<'input> {
    #[serde(rename = "type")]
    pub type_field: &'input str,
    pub version: &'input str,
    #[serde(borrow)]
    #[serde(rename = "CityObjects")]
    pub city_objects: IndexMap<&'input str, Cityobject<'input>>,
    pub vertices: Vec<[u32; 3]>,
    pub metadata: Metadata<'input>,
    pub transform: Transform,
}

#[derive(DeepSizeOf, Clone, Debug, Deserialize, PartialEq, Serialize)]
#[serde(deny_unknown_fields)]
#[serde(rename_all = "camelCase")]
pub struct Metadata<'input> {
    pub geographical_extent: Vec<f64>,
    pub reference_system: &'input str,
}

#[derive(DeepSizeOf, Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Transform {
    pub scale: [f64; 3],
    pub translate: [f64; 3],
}

#[derive(DeepSizeOf, Clone, Debug, Deserialize, PartialEq, Serialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
pub enum Multidimensionalarray {
    MultiSurface(Vec<[[u32; 3]; 1]>),
    Solid([Vec<[[u32; 3]; 1]>; 1]),
    Solid2([[Vec<[[u32; 3]; 1]>; 1]; 1]),
}

#[derive(DeepSizeOf, Clone, Debug, Deserialize, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct Geometry<'input> {
    #[serde(rename = "type")]
    pub type_field: &'input str,
    pub lod: &'input str,
    pub boundaries: Multidimensionalarray,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct Cityobject<'input> {
    pub attributes: Attributes<'input>,
    pub geometry: Vec<Geometry<'input>>,
    #[serde(rename = "type")]
    pub type_field: &'input str,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Attributes<'input> {
    #[serde(deserialize_with = "bool::parse", default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aanduidingrecordcorrectie: Option<bool>,
    #[serde(deserialize_with = "bool::parse", default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aanduidingrecordinactief: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aantal_punten: Option<u32>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bagpandid: Option<&'input str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub baseheight: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "realnaivedate::parse", default)]
    pub begindatumtijdvakgeldigheid: Option<NaiveDate>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bgt_status: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bgt_functie: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bgt_type: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bgt_fysiekvoorkomen: Option<&'input str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "year::parse", default)]
    pub bouwjaar: Option<i16>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bronhouder: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub namespace: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub plus_functiewegdeel: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub plus_functie: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub plus_status: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub plus_type: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub plus_fysiekvoorkomen: Option<&'input str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "realnaivedatetime::parse", default)]
    pub tijdstipregistratie: Option<NaiveDateTime>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lokaalid: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub eindregistratie: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identificatienamespace: Option<&'input str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "realnaivedatetime::parse", default)]
    pub objectbegintijd: Option<NaiveDateTime>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "realnaivedatetime::parse", default)]
    pub objecteindtijd: Option<NaiveDateTime>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hoortbijtypeoverbrugging: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub typeoverbruggingsdeel: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub overbruggingisbeweegbaar: Option<&'input str>,
    #[serde(deserialize_with = "bool::parse", default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ondersteunendwegdeeloptalud: Option<bool>,
    #[serde(deserialize_with = "bool::parse", default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub onbegroeidterreindeeloptalud: Option<bool>,
    #[serde(deserialize_with = "bool::parse", default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub begroeidterreindeeloptalud: Option<bool>,
    #[serde(deserialize_with = "bool::parse", default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wegdeeloptalud: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "realnaivedate::parse", default)]
    pub p_punt_datum: Option<NaiveDate>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "realnaivedatetime::parse", default)]
    pub lv_publicatiedatum: Option<NaiveDateTime>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "realnaivedate::parse", default)]
    pub documentdatum: Option<NaiveDate>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub documentnummer: Option<Cow<'input, str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(deserialize_with = "realnaivedate::parse", default)]
    pub einddatumtijdvakgeldigheid: Option<NaiveDate>,
    #[serde(deserialize_with = "bool::parse", default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inonderzoek: Option<bool>,
    #[serde(rename = "3df_class")]
    pub n3_df_class: &'input str,
    #[serde(rename = "3df_id")]
    pub n3_df_id: &'input str,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_3posit5: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub nummeraanduidingreeks_2ident: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_1ident1: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_1posit1: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_1posit3: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_1posit5: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_2ident1: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub nummeraanduidingreeks_2tekst: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_2posit1: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_2posit3: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_2posit5: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub nummeraanduidingreeks_1tekst: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_3ident1: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_3posit3: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numaanduidingreeks_3posit1: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub nummeraanduidingreeks_1ident: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub nummeraanduidingreeks_3ident: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub nummeraanduidingreeks_3tekst: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identificatiebagpnd: Option<&'input str>,
    #[serde(deserialize_with = "objectid::parse")]
    pub objectid: u32,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub officieel: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pandstatus: Option<&'input str>,
    #[serde(deserialize_with = "float::parse", default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub relatievehoogteligging: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub roofheight: Option<f64>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shape_area: Option<&'input str>,
    #[serde(borrow)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shape_length: Option<&'input str>,
}
