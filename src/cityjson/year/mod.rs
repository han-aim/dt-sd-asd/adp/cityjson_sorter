use serde::{
    de::{Deserializer, Unexpected},
    Deserialize,
};

type NumYear = i16;

#[derive(Clone, Copy, Debug, PartialEq, Deserialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
enum Year<'input> {
    Standard(Option<NumYear>),
    #[serde(borrow)]
    Nonstandard(&'input str),
}

/// # Errors
///
/// When parsed `&str` does not contain a year representable as `NumYear`.
pub fn parse<'de, D>(deserializer: D) -> Result<Option<NumYear>, D::Error>
where
    D: Deserializer<'de>,
{
    return match Year::deserialize(deserializer)? {
        Year::Standard(yearnum) => Ok(yearnum),
        Year::Nonstandard(str) => str.parse::<NumYear>().map(Option::Some).map_err(|err| {
            serde::de::Error::invalid_value(Unexpected::Str(err.to_string().as_str()), &"A year.")
        }),
    };
}
