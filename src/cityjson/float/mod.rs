use serde::{
    de::{Deserializer, Unexpected},
    Deserialize,
};

#[derive(Clone, Copy, Debug, PartialEq, Deserialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
enum Float<'input> {
    Standard(f64),
    #[serde(borrow)]
    Nonstandard(&'input str),
}

/// # Errors
///
/// Fails to parse as `f64`.
pub fn parse<'de, D>(deserializer: D) -> Result<Option<f64>, D::Error>
where
    D: Deserializer<'de>,
{
    return Ok(match Float::deserialize(deserializer)? {
        Float::Standard(float) => Some(float),
        Float::Nonstandard(str) => match str.parse::<f64>() {
            Ok(okfloat) => Some(okfloat),
            Err(_) => {
                return if str.is_empty() {
                    Ok(None)
                } else {
                    Err(serde::de::Error::invalid_value(
                        Unexpected::Str(str),
                        &"Parseable as a float. ",
                    ))
                };
            }
        },
    });
}
