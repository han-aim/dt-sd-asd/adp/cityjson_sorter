use chrono::{naive::NaiveTime, NaiveDate, NaiveDateTime};
use serde::{
    de::{Deserializer, Unexpected},
    Deserialize,
};

#[derive(Clone, Copy, Debug, PartialEq, Deserialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
enum RealNaiveDateTime<'input> {
    Standard(Option<NaiveDateTime>),
    #[serde(borrow)]
    Nonstandard(&'input str),
}

/// # Errors
///
/// When parsed `&str` does not match date format.
fn naivedatetime_from_str(strok: &str) -> Result<Option<NaiveDateTime>, chrono::ParseError> {
    // Without fractional seconds.
    const FORMAT: &str = "%Y-%m-%dT%H:%M:%S%:z";
    const FORMAT2: &str = "%Y%m%d";
    const FORMAT3: &str = "%Y%m%d%H%M%S";
    if strok.is_empty() {
        Ok(None)
    } else {
        match NaiveDateTime::parse_from_str(strok, FORMAT) {
            Ok(naivedatetime) => Ok(Some(naivedatetime)),
            Err(_) => match NaiveDate::parse_from_str(strok, FORMAT2) {
                Ok(naivedatetime) => {
                    let zerotime = NaiveTime::from_hms(0, 0, 0);
                    Ok(Some(naivedatetime.and_time(zerotime)))
                }
                Err(_) => match NaiveDate::parse_from_str(strok, FORMAT3) {
                    Ok(naivedatetime) => {
                        let zerotime = NaiveTime::from_hms(0, 0, 0);
                        Ok(Some(naivedatetime.and_time(zerotime)))
                    }
                    Err(e) => Err(e),
                },
            },
        }
    }
}

/// # Errors
///
/// When parsed `&str` does not match date/time format.
pub fn parse<'de, D>(deserializer: D) -> Result<Option<NaiveDateTime>, D::Error>
where
    D: Deserializer<'de>,
{
    return match RealNaiveDateTime::deserialize(deserializer)? {
        RealNaiveDateTime::Standard(option_naivedatetime) => Ok(option_naivedatetime),
        RealNaiveDateTime::Nonstandard(str) => naivedatetime_from_str(str).map_err(|err| {
            serde::de::Error::invalid_value(
                Unexpected::Str(err.to_string().as_str()),
                &"A date with time.",
            )
        }),
    };
}
