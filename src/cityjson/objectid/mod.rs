type NumObjectid = u32;

use serde::{
    de::{Deserializer, Unexpected},
    Deserialize,
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, Deserialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
enum Objectid<'input> {
    Standard(NumObjectid),
    #[serde(borrow)]
    Nonstandard(&'input str),
}

/// # Errors
///
/// Fails to parse other types than `&str` or `NumObjectid`.
pub fn parse<'de, D>(deserializer: D) -> Result<NumObjectid, D::Error>
where
    D: Deserializer<'de>,
{
    return Ok(match Objectid::deserialize(deserializer)? {
        Objectid::Standard(objectid) => objectid,
        Objectid::Nonstandard(string) => match string.parse::<NumObjectid>() {
            Ok(objectidnum) => objectidnum,
            Err(_) => {
                return Err(serde::de::Error::invalid_value(
                    Unexpected::Str(string),
                    &"Parseable as an u32. ",
                ));
            }
        },
    });
}
