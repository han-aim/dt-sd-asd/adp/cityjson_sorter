use chrono::NaiveDate;
use serde::{
    de::{Deserializer, Unexpected},
    Deserialize,
};

#[derive(Clone, Copy, Debug, PartialEq, Deserialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
enum RealNaiveDate<'input> {
    Standard(Option<NaiveDate>),
    #[serde(borrow)]
    Nonstandard(&'input str),
}

/// # Errors
///
/// When string does not match date format.
pub fn parse<'de, D>(deserializer: D) -> Result<Option<NaiveDate>, D::Error>
where
    D: Deserializer<'de>,
{
    const FORMAT: &str = "%Y%m%d";
    return match RealNaiveDate::deserialize(deserializer)? {
        RealNaiveDate::Standard(option_naivedate) => Ok(option_naivedate),
        RealNaiveDate::Nonstandard(str) => NaiveDate::parse_from_str(str, FORMAT)
            .map(Option::Some)
            .map_err(|err| {
                serde::de::Error::invalid_value(
                    Unexpected::Str(err.to_string().as_str()),
                    &"A date.",
                )
            }),
    };
}
