use serde::{
    de::{Deserializer, Unexpected},
    Deserialize,
};

#[derive(Clone, Copy, Debug, PartialEq, Deserialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
enum Bool<'input> {
    Standard(bool),
    #[serde(borrow)]
    Nonstandard(&'input str),
}

/// # Errors
///
/// Fails to parse other types than `str` or `bool`.
pub fn parse<'de, D>(deserializer: D) -> Result<Option<bool>, D::Error>
where
    D: Deserializer<'de>,
{
    return Ok(match Bool::deserialize(deserializer)? {
        Bool::Standard(okbool) => Some(okbool),
        Bool::Nonstandard("true" | "1") => Some(true),
        Bool::Nonstandard("false" | "0" | "") => Some(false),
        Bool::Nonstandard(other) => {
            return Err(serde::de::Error::invalid_value(
                Unexpected::Str(other),
                &"Truthy value.",
            ))
        }
    });
}
